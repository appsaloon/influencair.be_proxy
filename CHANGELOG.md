# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.4] - 2020-12-14

### Changed

- turn off logger
- greenlock settings change

## [1.0.3] - 2020-09-11

### Changed

- upgrade node version
- add proxy endpoints

## [1.0.2] - 2020-06-03

### Changed

- update http-proxy-middleware

## [1.0.1] - 2020-03-25

### Changed

- update greenlock version
- refactor code

## [1.0.0] - 2019-11-11

### Changed

- updated greenlock version
- eslint styling
- updated node version
