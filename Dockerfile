FROM node:14-alpine AS release

WORKDIR /server
COPY package*.json ./

# install production dependencies
RUN npm i --only=prod

RUN apk add --no-cache tini
ENTRYPOINT ["/sbin/tini", "--"]

WORKDIR /server
COPY src/ ./

CMD ["node", "index"]