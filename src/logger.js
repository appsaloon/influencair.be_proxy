const initLogstash = require('@appsaloon/logger-js')
const logstashOptions = {
  protocol: 'https',
  hostname: 'elk.appsaloon.be',
  path: 'app-backend',
  site: 'influencair/proxy'
}
initLogstash.default(logstashOptions)
