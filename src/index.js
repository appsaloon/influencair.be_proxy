// require('./logger')
const app = require('./app')

const useLetsencryptProductionServer = (process.env && process.env.USE_LETSENCRYPT_PRODUCTION === 'true')
const production = (process.env.NODE_ENV === 'production')

if (production) {
  require('greenlock-express')
    .init(() => {
      return {
        packageRoot: './',
        maintainerEmail: 'toon@appsaloon.be',
        configDir: './greenlock.d',
        staging: !useLetsencryptProductionServer,
        notify: (ev, args) => {
          console.info(ev, args)
        }
      }
    })
    .serve((glx) => glx.serveApp(app))
} else {
  app.listen(80, () => console.info('app started at port 80'))
}
