const express = require('express')
const favicon = require('serve-favicon')
const compression = require('compression')
const { createProxyMiddleware } = require('http-proxy-middleware')
const path = require('path')

const app = express()

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))

app.use(compression())

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

app.use('/history_map', createProxyMiddleware({
  target: 'http://history-viewer',
  pathRewrite: { '^/history_map': '/' },
  router: {
    'history.influencair.be': 'http://history-viewer'
  }
}))

app.use('/', createProxyMiddleware({
  target: 'http://map',
  router: {
    'map.influencair.be': 'http://map',
    'data.influencair.be': 'http://data:8080',
    'history.influencair.be': 'http://history:8081',
    'datastory.influencair.be': 'http://datastory',
    'docs.influencair.be': 'http://docs:3000',
    'doc.influencair.be': 'http://docs:3000',
    'www.influencair.be': 'http://docs:3000',
    'influencair.be': 'http://docs:3000'
  }
}))

app.use((req, res, next) => {
  var err = new Error('Not Found')
  err.status = 404
  next(err)
})

app.use((err, req, res, next) => {
  console.error('Something went wrong:', err)
  res.status(err.status || 500).json({
    message: err.message,
    error: err
  })
})

module.exports = app
